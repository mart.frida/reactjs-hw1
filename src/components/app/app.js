import React from "react";
import Header from "../header/header";
import Numbers from "../numbers/numbers";
import { Project } from "../project/project";
import { Request } from "../request/request";
import "./app.css";

const App = () => {
  return (
    <div>
      <Header></Header>
      <Numbers></Numbers>
      <Project></Project>
      <Request></Request>
    </div>
  );
};

export default App;
