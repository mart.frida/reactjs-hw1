import React from "react";
import "./request.css";
import send  from "../../media/send.png";

export const Request = () => {
  return (
    <div class="request">
      <p>САМЫЕ УМНЫЕ ПРОЕКТЫ</p>
      <p>РЕАЛИЗУЕМ САМЫЕ СМЕЛЫЕ РЕШЕНИЯ</p>
      <button>
        <img src={send} alt="send" />
        ВАШ ЗАПРОС 
      </button>
    </div>
  );
};
