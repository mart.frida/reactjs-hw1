import React from "react";
import "./project-arena.css";
import arena from '../../media/Rectangle3.png'

export const ProjectArena = () => {
  return (
    <div class="project-arena">
      <img src={arena} alt="arena"/>
      <div class="line1"></div>
      <h3>Арена</h3>
      <p>
        Мы сделали самую красивую арену в Европе. Это открытие стало для нас
        прорывной точкой для разивтия на следующие десятилетия. Мы очень рады
        данному еву.
      </p>
    </div>
  )
}
