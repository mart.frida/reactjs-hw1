import React from "react";
import { ProjectArena } from "../project-arena/project-arena";
import "./project.css";

export const Project = () => {
  return (
    <div className="project">
      <h2>НАШИ САМЫЕ БОЛЬШИЕ ПРОЕКТЫ</h2>
      <ProjectArena></ProjectArena>
      <ProjectArena></ProjectArena>
      <ProjectArena></ProjectArena>
    </div>
  );
};
