import React from "react";
import HeaderContent from "../header-content/header-content";
import "./header.css"

const Header = () => {
  return <header>
    <HeaderContent></HeaderContent>
  </header>;
};

export default Header;
